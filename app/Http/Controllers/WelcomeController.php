<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use Illuminate\Support\Facades\Input;

class WelcomeController extends Controller
{
    //
    public function index(){
        
        $allBooks = Book::with(['authorz','bookStock'])->get();       //dd($allBooks);
        $counter = 0;
        return view('welcome', compact('allBooks','counter'));
    }

    // function for deleting the Record
    public function deleteBook(){
        $bookID = Input::get('id');

        $obj = Book::find($bookID);
        //deleting the book form DB
        Book::destroy($bookID);
        // deleting the relations
        $obj->authorz()->detach();
        $obj->bookStock()->detach();

        return response($bookID);
    }
}
