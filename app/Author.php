<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Book;

class Author extends Model
{
    //
    public function boks(){
        return $this->hasMany(Book::class);
    }
}
