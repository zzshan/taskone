<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Author;
use App\BookStock;

class Book extends Model
{
    // having relation for many books belongs to many authors
    public function authorz(){
        return $this->belongsToMany(Author::class);
    }

    // 
    public function bookStock(){
        return $this->belongsToMany(BookStock::class);
        //return $this->belongsToMany(BookStock::class)
        //->selectRaw('count(book_id) as countt')
        //->groupby('book_id');
    }
}
