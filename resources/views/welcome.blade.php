<!DOCTYPE html>
<html lang="en">

<head>
    <title>Author & Books Stocks</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <h2>List of Books in Stocks</h2>
        <p>Books could be delete by using Delete Button in Action</p>
        <div class="alert alert-success" style="display:none;" id="ajaxAlerts" role="alert">
  Successfully Deleted Book Record....!
</div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Book Name</th>
                    <th>Author</th>
                    <th>Price</th>
                    <th>Stock In Hands</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($allBooks as $book)
                <tr id="row{{++$counter}}">
                    <td>{{$book->id}}</td>
                    <td>{{$book->name}}</td>
                    <td>{{$book->authorz[0]->name}}</td>
                    <td>{{$book->price}}.$</td>
                    <td>{{count($book->bookStock)}}</td>
                    <td><span onclick="deleteAction({{$book->id}},{{$counter}})" class="btn btn-danger">Delete</span> </td>
                </tr>
                @empty
                <tr>Oopss..! No Record Found.</tr>
                @endforelse
            </tbody>
        </table>
    </div>

</body>

<script type="text/javascript">
    // Function to delete the Property
    function deleteAction(bid = 0, counter = 0) {
        if (confirm('Are you sure to want to move this Book to TRASH....!')) {
            $.ajax({
                url: 'delete-book',
                data: {
                    id: bid
                },
                dataType: 'html',
                success: function(response) {
                    console.log('success---'+ response);
                    $('#row' + counter).fadeOut(2000);
                    $('#ajaxAlerts').show().fadeOut(4000);
                },
                error: function() {
                    alert('Error....!');
                }
            });
        }
        return false;
    };
</script>

</html>